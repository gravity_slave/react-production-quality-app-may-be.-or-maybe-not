import React, { Component } from 'react';
import logo from './logo.svg';
import './App.scss';
import {TodoForm, TodoList, Footer} from './react_components/todo';
import {addTodo, generateId, findById, toggleTodo, updateTodo, removeTodo, filterTodos} from  './lib/todoHelpers';
import {pipe, partial} from './lib/utils';
import {loadTodos, createTodo, revampTodo, deleteTodo} from './lib/todoService';
class App extends Component {
    state = {
    todos: [],
    currentTodo: '',
    errorMsg: '',
    message: ''
};

    static contextTypes = {
      route: React.PropTypes.string
    };

    componentDidMount() {
        loadTodos()
            .then(todos => this.setState({ todos })
    )
    }


    handleToggle = (id) => {
        const getToggledTodo = pipe(findById,toggleTodo);
        const updated = getToggledTodo(id, this.state.todos);
        const getUpdatedTodos = partial(updateTodo, this.state.todos);
        // const todo = findById(id, this.state.todos);
        // const toggled  = toggleTodo(todo);
        // const updatedTodos = updateTodo(this.state.todos, toggled);
        const updatedTodos = getUpdatedTodos(updated);
        this.setState({
            todos: updatedTodos
        });
        revampTodo(updated)
            .then(() => this.showTempMsg('Todo has been successfully updated'));

    };

    handleInputChange = (event) => {
        this.setState({
            currentTodo: event.target.value
        }, () => {
            console.log(this.state.currentTodo);
        });

    };

    handleEmptySubmit = (evt) => {
        evt.preventDefault();
        this.setState({
            errorMsg: 'please, supply a name',
        });

    };
    handleSubmit = (evt) => {
        evt.preventDefault();

        const newId = generateId();
        console.log(`new id is ${newId}`);
        const newTodo = {
            id: newId,
            name: this.state.currentTodo,
            isCompleted: false
        };
        const updatedTodos = addTodo(this.state.todos, newTodo);
        this.setState({
            todos: updatedTodos,
            currentTodo: '',
            errorMsg: ''
        });
        createTodo(newTodo)
            .then(res => this.showTempMsg('Todo was successfully created'));
    };

    handleRemove = (id, evt) => {
        evt.preventDefault();
        const updatedTodos = removeTodo(this.state.todos, id);
        this.setState({ todos: updatedTodos });
        deleteTodo(id)
            .then(() => this.showTempMsg('Todo has been removed!'));

    };
    showTempMsg = (message) => {
        this.setState({message});
        setTimeout( () => {
            this.setState({message: ''})
        }, 2500);
    };
  render() {
      const submitHandler = this.state.currentTodo ? this.handleSubmit : this.handleEmptySubmit;
      const displayTodos = filterTodos(this.state.todos, this.context.route);
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>React Todos</h2>
        </div>
       <div className="Todo-App">
           {this.state.errorMsg && <span className="error">{this.state.errorMsg}</span>}
           {this.state.message && <span className="success">{this.state.message}</span>}

           <TodoForm
               currentTodo={this.state.currentTodo}
               handleInputChange={this.handleInputChange}
               handleSubmit={submitHandler} >


           </TodoForm>
           <TodoList
               todos={displayTodos}
               handleToggle={this.handleToggle}
               handleRemove={this.handleRemove}
           />

           <Footer/>

       </div>
       </div>
    );
  }
}

export default App;
